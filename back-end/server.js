import express from 'express';
import data from './data.js';

//  creating exprees app or defining app and return express object 
const app = express();

// API request [SECTION] retirv produc as defaul 
app.get('/api/products', (req, res) => {
    res.send(data.products)

})


// API request [SECTION] for indivdual product
app.get('/api/products/slug/:slug', (req, res) => {
    const product = data.products.find(x => x.slug === req.params.slug);
    if (product) {
        res.send(product);
    } else {
        res.status(404).send({message: 'Product not found'});
    }


})


// defien the port express 
const port = process.env.PORT || 5000;
app.listen(port, () => {
    console.log(`server at http://localhost:${port}`);


});